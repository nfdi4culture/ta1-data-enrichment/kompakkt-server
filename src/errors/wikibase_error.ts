class WikibaseError extends Error {
    constructor(message: string) {
        // message is a Json string
        // we try to find the key "message" anywhere in the string
        // it can be in the root of the json or in a nested object
        // if we find it, we use it as the error message
        // otherwise we use the whole string as the error message
        try {
            const json = JSON.parse(message);
            if (json.message) {
                message = json.message;
            } else {
                for (const key in json) {
                    if (json[key].message) {
                        message = json[key].message;
                        break;
                    }
                }
            }
        } catch (e) {
            // do nothing
        }

        super(message);
        this.name = "WikibaseError";
    }
}

export default WikibaseError;
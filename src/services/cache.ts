import Redis from 'ioredis';
import hash from 'object-hash';
import { Logger } from './logger';
import { Configuration } from './configuration';

const { Hostname: host, Port: port, DBOffset: offset, Disabled: disabled } = Configuration.Redis;

class CacheClient {
  private redis: Redis.Redis;
  private db: number;
  private defaultSeconds: number;
  public hash = hash;

  constructor(db: number, defaultSeconds = 60) {
    this.db = db;
    this.redis = new Redis({ db, host, port });
    this.defaultSeconds = defaultSeconds;
    Logger.log(`Initialized Redis using DB ${db}`);
  }

  get client() {
    return this.redis;
  }

  public async flush() {
    if (disabled) return;
    return this.redis.flushdb().then(() => Logger.log(`Flushed Redis DB ${this.db}`));
  }

  public async del(key: string) {
    if (disabled) return;
    return this.redis.del(key);
  }

  public async has(key: string) {
    if (disabled) return false;
    return this.redis.get(key).then(value => !!value);
  }

  public async get<T extends unknown>(key: string) {
    if (disabled) return undefined;
    return this.redis.get(key).then(value => (value ? (JSON.parse(value) as T) : undefined));
  }

  public async set(key: string, value: any, seconds = this.defaultSeconds) {
    if (disabled) return;
    return this.redis.set(key, JSON.stringify(value), 'EX', seconds);
  }
}

// Repo/Entity MongoDB Cache
const RepoCache = new CacheClient(offset + 1);
// User/Account MongoDB Cache
const UserCache = new CacheClient(offset + 2);
// User/Account Session Cache
const SessionCache = new CacheClient(offset + 3);
// Cache information about who uploaded files
const UploadCache = new CacheClient(offset + 4, 3.6e4);
// Possible metadata choices
// const MetadataChoicesCache = new CacheClient(offset + 5, 3.6e4);

// the timeout for the next two caches are pretty important
// the need to be larger than the update latency of the wikibase
// update script (the one moving written data to the triplet store)
// but also slow, so manual changes to wikibase data get updated properly
// in kompakkt.

// Possible metadata choices
const MetadataChoicesCache = new CacheClient(offset + 5, 3.6e4);
// Digital Entity MongoDB Cache
const DigitalEntityCache = new CacheClient(offset + 6, 3.6e4);
// Digital Entity Wikibase Cache
const DigitalEntityWikibaseCache = new CacheClient(offset + 7, 3.6e4);
// Annotation Wikibase Cache
const AnnotationWikibaseCache = new CacheClient(offset + 8, 3.6e4);

export { RepoCache,
         UserCache,
         SessionCache,
         UploadCache,
         CacheClient,
         MetadataChoicesCache,
         DigitalEntityCache,
         DigitalEntityWikibaseCache,
         AnnotationWikibaseCache,
       };

// prettier-ignore
import { ICompilation, IAddress, IEntity, IContact, IMediaAgent, IWikibaseItem, IDigitalEntity, IInstitution, ITag, IAnnotation, isDigitalEntity, isInstitution, isUnresolved } from '../../common';
import { IDigitalEntityKompakkt,
         IDigitalEntityWikibase,
         IAnnotationKompakkt,
         IAnnotationWikibase,
         mergeAnnotation
} from './definitions';
import Entities from './entities';
import { fetchWikibaseMetadata, fetchAnnotation } from './wikibase';
import { DigitalEntityWikibaseCache, AnnotationWikibaseCache } from '../cache';


const removeUnrelatedEntities = <T extends unknown>(
  entityId: string,
) => {
  // const relatedRole = obj.roles[entityId];
  // obj.roles = {};
  // if (relatedRole) obj.roles[entityId] = relatedRole;
  // if (isPerson(obj)) {
  //   const relatedInst = obj.institutions[entityId];
  //   obj.institutions = {};
  //   if (relatedInst) obj.institutions[entityId] = relatedInst;
  //   const relatedContact = obj.contact_references[entityId];
  //   obj.contact_references = {};
  //   if (relatedContact) obj.contact_references[entityId] = relatedContact;
  // } else if (isInstitution(obj)) {
  //   const relatedAddress = obj.addresses[entityId];
  //   obj.addresses = {};
  //   if (relatedAddress) obj.addresses[entityId] = relatedAddress;
  //   const relatedNote = obj.notes[entityId];
  //   obj.notes = {};
  //   if (relatedNote) obj.notes[entityId] = relatedNote;
  // }
  // return obj as T;
};

const exists = (obj: any) => !!obj;

const resolveInstitution = async (institution: IInstitution, entityId?: string) => {
  // if (entityId) institution = removeUnrelatedEntities<IInstitution>(institution, entityId);
  //
  // for (const [id, address] of Object.entries(institution.addresses)) {
  //   // Next line is due to migration 0001
  //   // Can be removed once all institutions are migrated using 0001
  //   if (!isUnresolved(address)) continue;
  //   const resolved = await Entities.resolve<IAddress>(address, 'address');
  //   if (resolved) institution.addresses[id] = resolved;
  //   else delete institution.addresses[id];
  // }
  //
  // return institution;
};

const resolvePerson = async (person: IMediaAgent, entityId?: string) => {
  // if (entityId) person = removeUnrelatedEntities<IPerson>(person, entityId);
  //
  // for (const [id, contact] of Object.entries(person.contact_references)) {
  //   // Next line is due to migration 0001
  //   // Can be removed once all persons are migrated using 0001
  //   if (!isUnresolved(contact)) continue;
  //   const resolved = await Entities.resolve<IContact>(contact, 'contact');
  //   if (resolved) person.contact_references[id] = resolved;
  //   else delete person.contact_references[id];
  // }
  //
  // for (const [id, institutions] of Object.entries(person.institutions)) {
  //   const resolvedInstitutions = await Promise.all(
  //     (institutions ?? []).map(async i =>
  //       Entities.resolve<IInstitution>(i, 'institution').then(resolved => {
  //         if (!resolved) return undefined;
  //         return resolveInstitution(resolved, entityId);
  //       }),
  //     ),
  //   );
  //   person.institutions[id] = resolvedInstitutions.filter(i => isInstitution(i)) as IInstitution[];
  // }
  //
  // return person;
};

const resolveDigitalEntity = async (digitalEntity: IDigitalEntityKompakkt) => {
  const entity = { ... digitalEntity } as any;
  // let wb_data = (await fetchWikibaseMetadata(entity.wikibase_id));
  let wb_data =
    (await DigitalEntityWikibaseCache.get<IDigitalEntityWikibase>(entity.wikibase_id)) ??
    (await fetchWikibaseMetadata(entity.wikibase_id));

  // if (!wb_data) return undefined;

  if (!wb_data) return undefined;
  await DigitalEntityWikibaseCache.set(entity.wikibase_id, wb_data);

  // commented out to allow metadata edited in the wikibase frontend to show up after cache invalidation period
  // await DigitalEntityWikibaseCache.set(entity.wikibase_id, wb_data);

  delete (wb_data as any).id;
  delete (wb_data as any).internalID;
  return {
      ...entity,
      ...wb_data
  } as IDigitalEntity;
  // const entity = (await resolveMetaDataEntity(digitalEntity)) as IDigitalEntity;
  //
  // entity.tags = (
  //   await Promise.all((entity.tags ?? []).map(tag => Entities.resolve<ITag>(tag, 'tag')))
  // ).filter(exists) as ITag[];
  //
  // if (entity.phyObjs) {
  //   for (let i = 0; i < entity.phyObjs.length; i++) {
  //     const resolved = await Entities.resolve<IPhysicalEntity>(entity.phyObjs[i], 'physicalentity');
  //     if (!resolved) {
  //       continue;
  //     }
  //     entity.phyObjs[i] = (await resolveMetaDataEntity(resolved)) as IPhysicalEntity;
  //   }
  // }
  //
  // return entity;
};

const resolveEntity = async (entity: IEntity) => {
  for (const id in entity.annotations) {
    // the expected return type of this is IAnnotation, but the shallow result inside
    // resolve() yields an intermittent IAnnotationKompakkt turned into an IAnnotation
    // by the deep resolve. We could use a resolve<any>(..) as IAnnotation here but that
    // seems less clean
    const resolved = await Entities.resolve<IAnnotation | IAnnotationKompakkt>(id, 'annotation');
    if (!resolved) {
      delete entity.annotations[id];
      continue;
    }
    entity.annotations[id] = resolved;
  }

  if (!isDigitalEntity(entity.relatedDigitalEntity) && entity.relatedDigitalEntity !== undefined) {
    const id = entity.relatedDigitalEntity._id as string;
    if (id !== undefined && id.length > 0) {
      // const resolved = await Entities.fetchWikibaseMetadata(id);
      const resolved = await Entities.resolve<IDigitalEntity>(
        entity.relatedDigitalEntity,
        'digitalentity',
      );
      if (resolved) entity.relatedDigitalEntity = resolved;
    }
  }
  return entity;
};

const resolveCompilation = async (compilation: ICompilation) => {
  for (const id in compilation.entities) {
    const resolved = await Entities.resolve<IEntity>(id, 'entity');
    if (!resolved) {
      delete compilation.entities[id];
      continue;
    }
    compilation.entities[id] = resolved;
  }

  for (const id in compilation.annotations) {
    // the expected return type of this is IAnnotation, but the shallow result inside
    // resolve() yields an intermittent IAnnotationKompakkt turned into an IAnnotation
    // by the deep resolve. We could use a resolve<any>(..) as IAnnotation here but that
    // seems less clean
    const resolved = await Entities.resolve<IAnnotation | IAnnotationKompakkt>(id, 'annotation');
    if (!resolved) {
      delete compilation.annotations[id];
      continue;
    }
    compilation.annotations[id] = resolved;
  }

  return compilation;
};

const resolveAnnotation = async (annotation: IAnnotation | IAnnotationKompakkt) => {
  const kp = annotation as IAnnotationKompakkt;
  let wb : IAnnotationWikibase | undefined = undefined;
  if (kp.wikibase_id !== undefined && kp.wikibase_id !== null) {
    wb = (await AnnotationWikibaseCache.get<IAnnotationWikibase>(kp.wikibase_id)) ??
         (await fetchAnnotation(kp.wikibase_id));
    // commented out to allow metadata edited in the wikibase frontend to show up after cache invalidation period
    await AnnotationWikibaseCache.set(kp.wikibase_id, wb);
  }

  return mergeAnnotation(wb, kp);
};

export const Resolve = {
  institution: resolveInstitution,
  person: resolvePerson,
  digitalentity: resolveDigitalEntity,
  entity: resolveEntity,
  compilation: resolveCompilation,
  annotation: resolveAnnotation,
  get: <T extends unknown>(collection: string, ...args: any) =>
    (Resolve as any)[collection]
      ? ((Resolve as any)[collection](...args) as Promise<T | undefined>)
      : undefined,
};

// prettier-ignore
import { IAddress,
         IAgent,
         IAnnotation,
         IBody,
         ICameraPerspective,
         ICreationTuple,
         ICompilation,
         IContact,
         IContent,
         IDescriptionValueTuple,
         IDigitalEntity,
         IDimensionTuple,
         IDocument,
         IEntity,
         IFile,
         IGroup,
         IInstitution,
         IMediaAgent,
         IMediaHierarchy,
         ISelector,
         ISource,
         ITag,
         ITarget,
         ITypeValueTuple,
         IUserData,
         IVector3,
         IWikibaseItem,
         Collection,
} from '../../common';
import { Logger } from '../logger';
import { ObjectId } from 'mongodb';

const collections = Object.values(Collection);

Logger.info(`Defined collections: ${collections.join(', ')}`);

export type CollectionName =
  | 'address'
  | 'annotation'
  | 'compilation'
  | 'contact'
  | 'digitalentity'
  | 'entity'
  | 'group'
  | 'institution'
  | 'metadata'
  | 'annotationlink'
  | 'tag';

export const isValidCollection = (obj: any): obj is CollectionName => {
  return collections.includes(obj.toString());
};

// TODO: limit to entries which are pushable
export type PushableEntry =
  | IAddress
  | IAnnotation
  | IAnnotationKompakkt
  | ICompilation
  | IContact
  | IDigitalEntity
  | IDigitalEntityKompakkt
  | IEntity
  | IGroup
  | IInstitution
  | ITag;

export function getPushableEntryId(entry : PushableEntry) {
  const erased = entry as any;
  return erased._id ?? erased.id ?? '';
}

export function setPushableEntryId(entry : PushableEntry, id: ObjectId) {
  const erased = entry as any;
  if (erased._id !== undefined) {
    erased._id = id
  } else if (erased.id !== undefined) {
    erased.id = id
  }
}

export interface ICollectionParam {
  collection: CollectionName;
}

export interface IEntityHeadsUp {
  headsUp: {
    user: IUserData;
    doesEntityExist: boolean;
    isValidObjectId: boolean;
    collectionName: CollectionName;
  };
}

export interface IMailEntry {
  _id: string | ObjectId;
  target: string;
  content: {
    mailbody: string;
    subject: string;
  };
  timestamp: string;
  user: IUserData;
  answered: boolean;
  mailSent: boolean;
}

export interface IDigitalEntityWikibase extends IWikibaseItem {
  agents: IMediaAgent[];
  techniques: IWikibaseItem[];
  software: IWikibaseItem[];
  equipment: string[];
  creationDate: string | undefined;
  externalLinks: string[];
  bibliographicRefs: IWikibaseItem[];
  physicalObjs: IWikibaseItem[];
  licence: number;
  hierarchies: IMediaHierarchy[],
}

export interface IDigitalEntityKompakkt extends IDocument {
  externalId: ITypeValueTuple[];
  externalLink: IDescriptionValueTuple[];
  biblioRefs: IDescriptionValueTuple[];
  other: IDescriptionValueTuple[];

  metadata_files: IFile[];

  type: string;

  discipline: string[];
  tags: ITag[];

  dimensions: IDimensionTuple[];
  creation: ICreationTuple[];
  files: IFile[];

  statement: string;
  objecttype: string;

  wikibase_id?: string;
  wikibase_address?: string;
  claims?: { [key: string] : any };
}

export interface IAnnotationWikibase extends IWikibaseItem {
  validated: boolean;
  ranking: number;
  created: string;
  motivation: string;
  lastModificationDate?: string;

  descriptionAuthors: IWikibaseItem[];
  descriptionLicenses: IWikibaseItem[];

  cameraType: string;
  cameraPosition: IVector3;
  cameraTarget: IVector3;

  relatedMedia: IWikibaseItem[];
  relatedMediaUrls: string[];
  relatedEntities: IWikibaseItem[];

  selectorPoint: IVector3;
  selectorNormal: IVector3;
  targetMetadata: number;
}

export interface IAnnotationKompakkt extends IDocument {
  generated?: string;
  creator: IAgent;
  created: string;
  generator: IAgent;
  lastModifiedBy: IAgent;
  ranking: number;
  lastModificationDate?: string;

  bodyType: string;
  contentType: string;

  targetEntity: string;
  targetCompilation?: string;
  relatedPerspective: ICameraPerspective;
  selectorPoint: IVector3;
  selectorNormal: IVector3;

  wikibase_id?: string;
  claims?: { [key: string] : any };
}

export function
splitDigitalEntity(entity: IDigitalEntity) {
  return {
    wb: {
      id: 'dummy', // we will get that from the wikibase API
      label: entity.label,
      description: entity.description,
      agents: entity.agents,
      techniques: entity.techniques,
      software: entity.software,
      equipment: entity.equipment,
      creationDate: entity.creationDate,
      externalLinks: entity.externalLinks,
      bibliographicRefs: entity.bibliographicRefs,
      physicalObjs: entity.physicalObjs,
      licence: entity.licence,
    } as IDigitalEntityWikibase,
    kp: {
      _id: entity._id,
      externalId: entity.externalId,
      externalLink: entity.externalLink,
      biblioRefs: entity.biblioRefs,
      other: entity.other,
      metadata_files: entity.metadata_files,
      type: entity.type,
      discipline: entity.discipline,
      tags: entity.tags,
      dimensions: entity.dimensions,
      creation: entity.creation,
      files: entity.files,
      statement: entity.statement,
      objecttype: entity.objecttype,
    } as IDigitalEntityKompakkt,
  };
}

export function
splitAnnotation(ann : IAnnotation, targetMetadataItem? : number) {
  return {
    wb: {
      id: ann.wikibase_id,
      label: { en: ann.body.content.title },
      description: ann.body.content.description,
      descriptionAuthors: ann.body.content.descriptionAuthors,
      descriptionLicenses: ann.body.content.descriptionLicenses,
      media: ann.body.content.link,
      validated: ann.validated,
      ranking: ann.ranking,
      created: ann.created,
      motivation: ann.motivation,
      lastModificationDate: ann.lastModificationDate,
      cameraType: ann.body.content.relatedPerspective.cameraType,
      cameraPosition: ann.body.content.relatedPerspective.position,
      cameraTarget: ann.body.content.relatedPerspective.target,
      relatedMedia: ann.body.content.relatedMedia,
      relatedMediaUrls: ann.body.content.relatedMediaUrls,
      relatedEntities: ann.body.content.relatedEntities,
      selectorPoint: ann.target.selector.referencePoint,
      selectorNormal: ann.target.selector.referenceNormal,
      targetMetadata: targetMetadataItem,
    } as IAnnotationWikibase,
    kp: {
      _id: ann._id,
      generated: ann.generated,
      creator: ann.creator,
      created: ann.created,
      generator: ann.generator,
      lastModifiedBy: ann.lastModifiedBy,
      ranking: ann.ranking,
      lastModificationDate: ann.lastModificationDate,
      bodyType: ann.body.type,
      contentType: ann.body.content.type,
      relatedPerspective: ann.body.content.relatedPerspective,
      targetEntity: ann.target.source.relatedEntity,
      targetCompilation: ann.target.source.relatedCompilation,
      selectorPoint: ann.target.selector.referencePoint,
      selectorNormal: ann.target.selector.referenceNormal,
      wikibase_id: ann.wikibase_id,
    } as IAnnotationKompakkt,
  };
}

export function
mergeAnnotation(wb : IAnnotationWikibase | undefined,
                kp : IAnnotationKompakkt) {
  // we use redundant values in kp as fallback if wb === undefined
  return {
    _id: kp._id,
    validated: wb?.validated ?? true,
    identifier: kp._id,
    ranking: wb?.ranking ?? kp.ranking,
    creator: kp.creator,
    created: wb?.created ?? kp.created,
    generator: kp.generator,
    generated: kp.generated,
    motivation: wb?.motivation ?? 'defaultMotivation',
    lastModificationDate: wb?.lastModificationDate ?? kp.lastModificationDate,
    lastModifiedBy: kp.lastModifiedBy,

    body: {
      type: kp.bodyType,
      content: {
        type: kp.contentType,
        title: wb?.label['en'] ?? '',
        description: wb?.description ?? '',
        descriptionAuthors: wb?.descriptionAuthors || [],
        descriptionLicenses: wb?.descriptionLicenses || [],
        link: wb?.media,
        relatedPerspective: wb ? ({
          cameraType: wb.cameraType,
          position: wb.cameraPosition,
          target: wb.cameraTarget,
          preview: kp.relatedPerspective.preview,
        } as ICameraPerspective) : kp.relatedPerspective,
        relatedMedia: wb?.relatedMedia ?? [],
        relatedMediaUrls: wb?.relatedMediaUrls ?? [],
        relatedEntities: wb?.relatedEntities ?? [],
      } as IContent,
    } as IBody,
    target: {
      source: {
        relatedEntity: kp.targetEntity,
        relatedCompilation: kp.targetCompilation,
      } as ISource,
      selector: {
        referencePoint: wb?.selectorPoint ?? kp.selectorPoint,
        referenceNormal: wb?.selectorNormal ?? kp.selectorNormal,
      } as ISelector,
    } as ITarget,
    wikibase_id: wb?.id,
  } as IAnnotation;
}

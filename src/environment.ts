import { LogLevel } from './enums';


if(process.env.DOCKER_ENV === 'true'){
  const environmentVariables = {
    "Mongo": {
      "ClientURL": process.env.MONGO_URL
    },
    "Redis": {
      "Hostname": "redis",
      "Port": process.env.REDIS_PORT,
    },
    "Express": {
      "Host": "0.0.0.0",
      "Port": process.env.SERVER_PORT,
      "PassportSecret": process.env.SESSION_SECRET,
      "PublicIP": "localhost",
      "enableHTTPS": false,
      "SSLPaths": {
        "PrivateKey": "/etc/nginx/ssl/key.pem",
        "Certificate": "/etc/nginx/ssl/cert.pem",
      }
    },
    "Mailer": {
      "Host": process.env.MAIL_HOST,
      "Port": process.env.MAIL_PORT,
      "Target": {
        "contact": process.env.MAIL_TARGET_CONTACT,
        "upload": process.env.MAIL_TARGET_UPLOAD,
        "bugreport": process.env.MAIL_TARGET_BUGREPORT,
      },
      "Auth": {
        "User": process.env.MAIL_USER,
        "Pass": process.env.MAIL_PASS
      },
      "Secure": process.env.MAIL_SECURE === 'true'
    },
    "Wikibase": {
      "KompakktAddress": process.env.KOMP_ADDR ?? "https://localhost:4100/",
      "Public": process.env.WB_PUBLIC ?? "http://localhost:8900/",
      "Domain": process.env.WB_DOMAIN,
      "SPARQLEndpoint": process.env.WB_SPARQL_ENDPOINT,
      "Username": process.env.WB_USERNAME,
      "Password": process.env.WB_PASSWORD,
      "AdminUsername": process.env.WB_USERNAME,
      "AdminPassword": process.env.WB_PASSWORD,
    }
  }

  //write the environmentVariables to a configt file at ${__dirname}/config.json
  const fs = require('fs');
  fs.writeFileSync(`${__dirname}/config.json`, JSON.stringify(environmentVariables, null, 2));
}

const Environment = {
  verbose: false,
  rootDirectory: `${__dirname}`,
  configFile: `${__dirname}/config.json`,
  logLevel: LogLevel.All,
};

const Verbose = Environment.verbose;
const RootDirectory = Environment.rootDirectory;
const ConfigFile = Environment.configFile;

export { Environment, Verbose, RootDirectory, ConfigFile };

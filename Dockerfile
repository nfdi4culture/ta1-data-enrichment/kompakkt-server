# syntax=docker/dockerfile:1
# express backend server

FROM node:16

WORKDIR /app
COPY . /app

RUN npm i

CMD ["npm", "start"]
